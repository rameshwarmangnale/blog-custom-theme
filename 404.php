<?php
get_header();
?>

<main class="container !mx-auto my-32 text-center">
    <h1 class="text-9xl text-white shadow-404">404</h1>
    <h3 class="text-3xl font-medium my-4">We can't seem to find the page you are looking for.</h3>
    <a class="underline underline-offset-4 !mt-4" >
        Head back to the home page.
    </a>
</main>

<?php
get_footer();
?>