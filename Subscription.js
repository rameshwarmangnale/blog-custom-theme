jQuery(document).ready(function ($) {
  $(".sub_news").click(function (e) {
    e.preventDefault();

    var data = {
      action: "subscribe",
      email: $("#mail").val(), // We pass php values differently!
    };

    if ($("#mail").val() != "") {
      jQuery.post(ajax_object.ajaxurl, data, function (response) {
        const obj = JSON.parse(response);
        if (obj.status === "success") {
          $("<p class='success'>" + obj.message + "</p>").insertAfter(
            ".sub_news"
          );
        } else {
          $("<p class='error'>" + obj.message + "</p>").insertAfter(
            ".sub_news"
          );
        }
        //alert(obj.message);

        console.log(response);
      });
    }
  });
});
