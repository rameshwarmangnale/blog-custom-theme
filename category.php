<?php
get_header();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <link rel="icon" type="image/svg+xml" href="<?php echo get_template_directory_uri(); ?>/public/favicon.svg" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Udemy Static Template</title>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="<?php echo get_template_directory_uri(); ?>https://fonts.googleapis.com/css2?family=Pacifico&family=Rubik:wght@300;400;500;700&display=swap" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/public/index.css">
</head>

<body>
  <!-- Main Content -->
  <main class="container !mx-auto my-16 grid grid-cols-3 gap-16">
    <div class="col-span-2">
      <!-- Entry-->
      <article class="flex border-b border-b-gray-200 pb-10 mb-10">
        <div class="grid grid-cols-2 gap-4">
          <div class="col-span-2">
          <p>Blogs Posts</p>
            <?php
            while (have_posts()) {
              the_post();
              $imagepath = wp_get_attachment_image_src(
                get_post_thumbnail_id(),'large');
            ?>
              <div class="container-1">
                <div class="card-1">
                  <div class="img-container-1">
                  <?php if($imagepath[0]!=""){ ?>
                  <img src="<?php echo @$imagepath[0] ?>"/>
                  <?php } ?> 
                  </div>
                  <div class="card-content-1">
                    <h1 class="font-bold"><?php the_title(); ?> </h1>
                    <!-- <h1>From the Other Side of the World</h1> -->
                    <p class="excerpt">
                      <?php the_excerpt(); ?> </p>
                      <?php echo get_the_date(); ?>
                    <p class="author-1 font-bold"> <?php the_author(); ?></p>
                    <a href="<?php the_permalink(); ?>">
                    <input class="cursor-pointer button" type="button" value="read more"></a>
                  </div>
                </div>
              </div>
            <?php } ?>
            <div class="clear"></div>
            <div class="pagination flex justify-between">
            <?php wp_pagenavi(); ?>
            </div>

          </div>
        </div>
      </article>
      <!-- Entry-->
      <!-- Pagination-->
      <!-- <nav class="flex justify-between">
        <a class="rounded-md py-2 px-4 block transition-all hover:bg-gray-100" href="#">
          Prev Page
        </a>
        <a class="rounded-md py-2 px-4 block transition-all hover:bg-gray-100" href="#">
          Next Page
        </a>
      </nav> -->
    </div>
    <div>
      <!-- Popular tags-->
      <div class="sidebar-tags">
        <h6 class="text-xl font-medium mb-5">Blog Tags</h6>
        <a href="#">fashion</a>
        <a href="#">gadgets</a>
        <a href="#">online shopping</a>
        <a href="#">top brands</a>
        <a href="#">travel</a>
        <a href="#">cartzilla news</a>
        <a href="#">personal finance</a>
        <a href="#">tips &amp; tricks</a>
      </div>
      <!-- Trending posts-->
      <div class="mb-8">
        <h6 class="text-xl font-medium mb-5">Trending Posts</h6>
        <div class="flex mb-4 items-center">
          <!-- Post Image -->
          <a class="shrink-0" href="#">
            <img class="rounded-lg w-16" src="<?php echo get_template_directory_uri(); ?>/public/trending-01.jpg">
          </a>
          <div class="text-sm ml-4">
            <!-- Post Title -->
            <a class="block font-medium" href="blog-single.html">
              Retro Cameras are Trending. Why so Popular?
            </a>
            <!-- Post Author -->
            <span class="text-gray-500">
              by <a href="#">Andy Williams</a>
            </span>
          </div>
        </div>
        <div class="flex mb-4 items-center">
          <a class="shrink-0" href="#">
            <img class="rounded-lg w-16" src="<?php echo get_template_directory_uri(); ?>/public/trending-02.jpg">
          </a>
          <div class="text-sm ml-4">
            <a class="block font-medium" href="blog-single.html">
              Retro Cameras are Trending. Why so Popular?
            </a>
            <span class="text-gray-500">
              by <a href="#">Andy Williams</a>
            </span>
          </div>
        </div>
        <div class="flex mb-4 items-center">
          <a class="shrink-0" href="#">
            <img class="rounded-lg w-16" src="<?php echo get_template_directory_uri(); ?>/public/trending-03.jpg">
          </a>
          <div class="text-sm ml-4">
            <a class="block font-medium" href="blog-single.html">
              Retro Cameras are Trending. Why so Popular?
            </a>
            <span class="text-gray-500">
              by <a href="#">Andy Williams</a>
            </span>
          </div>
        </div>
      </div>
      <!-- Categories-->
      <div class="sidebar-categories">
        <h6 class="text-xl font-medium mb-5">Blog Categories</h6>
        <ul>
          <li><a href="#">Example Category</a></li>
          <li><a href="#">Example Category</a></li>
          <li><a href="#">Example Category</a></li>
          <li><a href="#">Example Category</a></li>
          <li><a href="#">Example Category</a></li>
        </ul>
      </div>
    </div>
  </main>
  <?php
  get_footer();
  ?>
</body>