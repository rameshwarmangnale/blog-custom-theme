<?php

register_nav_menus(
    array('primary-menu' => 'Top Menu')
);

add_theme_support('post-thumbnails');

add_theme_support('custom-header');

add_theme_support('custom-background');

add_post_type_support('page', 'excerpt')

?>

<?php

function your_theme_support()
{
    add_theme_support("title-tag");
}
add_action("after_setup_theme", "your_theme_support");

function your_custom_title($title)
{
    return 'Blog Template';
}
add_filter('pre_get_document_title', 'your_custom_title');


//Init Hook for the custom post type

add_action('init', 'register_stores_post_type');

function register_stores_post_type()
{

    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
        'excerpt', // post excerpt
        'custom-fields', // custom fields
        'comments', // post comments
        'revisions', // post revisions
        'post-formats', // post formats
    );

    $labels = array(
        'name' => _x('Stores', 'blog-textdomain', 'plural'),
        'singular_name' => _x('Store', 'blog-textdomain', 'singular'),
        'menu_name' => _x('Stores', 'blog-textdomain', 'admin menu'),
        'name_admin_bar' => _x('Stores', 'blog-textdomain', 'admin bar'),
        'add_new' => _x('Add New', 'blog-textdomain', 'add new'),
        'add_new_item' => __('blog-textdomain', 'Add New Stores'),
        'new_item' => __('blog-textdomain', 'New Stores'),
        'edit_item' => __('blog-textdomain', 'Edit Stores'),
        'view_item' => __('blog-textdomain', 'View Stores'),
        'all_items' => __('blog-textdomain', 'All Stores'),
        'search_items' => __('blog-textdomain', 'Search Stores'),
        'not_found' => __('blog-textdomain', 'No Stores found.'),
    );

    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'description' => 'Holds our Stores and specific data',
        'public' => true,
        'taxonomies' => array('stores_category', 'post_tag'),
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'can_export' => true,
        'capability_type' => 'post',
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'stores'),
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 36,
        'menu_icon' => 'dashicons-megaphone',
    );
    flush_rewrite_rules();
    register_post_type('stores', $args); // Register Post type
}

//add taxonomies for store
function taxonomies()
{
    $taxonomies = array();

    $taxonomies['stores_category'] = array(
        'hierarchical'  => true,
        'query_var'     => 'stores-category',
        'rewrite'       => array(
            'slug'      => 'stores_category',
            'show_in_rest'  => true
        ),
        'labels'            => array(
            'name'          => 'Stores Category',
            'singular_name' => 'Stores Category',
            'edit_item'     => 'Edit Stores Category',
            'update_item'   => 'Update Stores Category',
            'add_new_item'  => 'Add Stores Category',
            'new_item_name' => 'Add New Stores Category',
            'all_items'     => 'All Stores Category',
            'search_items'  => 'Search Stores Category',
            'popular_items' => 'Popular Stores Category',
            'separate_items_with_commas' => 'Separate Stores Categories with Commas',
            'add_or_remove_items' => 'Add or Remove Stores Categories',
            'choose_from_most_used' => 'Choose from most used categories',
        ),
        'show_admin_column' => true,
        'show_in_rest'  => true
    );

    foreach ($taxonomies as $name => $args) {
        register_taxonomy($name, array('stores'), $args);
    }
}
add_action('init', 'taxonomies');

//add widget 

function my_theme_widgets_init()
{
    register_sidebar(array(
        'name' => 'Custom Widget Area',
        'id' => 'custom-widget-area',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
}
add_action('widgets_init', 'my_theme_widgets_init');

//shortcode
// function my_custom_shortcode( $atts ) {
//     $atts = shortcode_atts(
//       array(
//         'message' => 'Hello World!',
//       ),
//       $atts,
//       'custom_shortcode'
//     );

//     return $atts['message'];
//   }
//   add_shortcode( 'custom_shortcode', 'my_custom_shortcode' );


// add_action('publish_post', 'post_update_function', 10, 3);
// add_action('publish_post', 'send_post_update_email');
// Function to send an email to subscribers when a new post is published

//Email Subscription
function send_post_update_email($post_ID) {
    $sitename = get_bloginfo('name');
    $title = get_the_title($post_ID);
    $subscribers = get_option('subscribers');

    // Make sure that there are subscribers before sending the email
    if (!empty($subscribers)) {
        // Prepare the email content
        $body = 'New post added on ' . $sitename . ': "' . $title . '"<br/>' . get_the_excerpt($post_ID) . '<br/><a href="' . get_post_permalink($post_ID) . '">Read more</a>';
        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: ' . $sitename . ' <support@example.com>');

        // Send the email to all subscribers
        $emails = explode(',', $subscribers);
        foreach ($emails as $email) {
            wp_mail($email, 'New post added on ' . $sitename, $body, $headers);
        }
    }
}
add_action('publish_post', 'send_post_update_email');

// Shortcode to allow users to subscribe to the newsletter
function newsletter_subscription_shortcode() {
    ob_start();
    ?>
    <p>Subscribe to our newsletter:</p>
    <input type='email' id='mail' class='email-box' /><input class='sub_news' type='submit' value='Subscribe'>
    <?php
    return ob_get_clean();
}
add_shortcode('news_letter', 'newsletter_subscription_shortcode');

// Enqueue JS script
function enqueue_subscription_script() {
    wp_enqueue_script('subscription_script', get_stylesheet_directory_uri() . '/Subscription.js', array('jquery'), null, true);
    wp_localize_script(
        'subscription_script',
        'ajax_object',
        array('ajaxurl' => admin_url('admin-ajax.php'))
    );
}
add_action('wp_enqueue_scripts', 'enqueue_subscription_script');

// Handle ajax request and send response
add_action('wp_ajax_subscribe', 'subscribe_callback');
add_action('wp_ajax_nopriv_subscribe', 'subscribe_callback');

function subscribe_callback() {
    $subscribers = get_option('subscribers');
    $email = sanitize_email($_POST['email']);

    // Check if the email is valid and not already subscribed
    if (!is_email($email)) {
        echo json_encode(array(
            'status' => 'error',
            'message' => 'Invalid email address'
        ));
    } elseif (strpos($subscribers, $email) !== false) {
        echo json_encode(array(
            'status' => 'error',
            'message' => 'You have already subscribed'
        ));
        } else {
            // display the new email to the list of subscribers
            update_option('subscribers', $subscribers . $email . ',');
            echo json_encode(array(
                'status' => 'success',
                'message' => 'You have been successfully subscribed to our newsletter'
            ));
        }
    
        wp_die();
    }
    
?>