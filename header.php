<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/svg+xml" href="<?php echo get_template_directory_uri(); ?>/public/favicon.svg" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php wp_title(); ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="<?php echo get_template_directory_uri(); ?>https://fonts.googleapis.com/css2?family=Pacifico&family=Rubik:wght@300;400;500;700&display=swap" rel="stylesheet"> 
    <link href="<?php echo get_template_directory_uri(); ?>/bootstrap-icons/bootstrap-icons.css" rel="stylesheet"> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/public/index.css">
    <link href="<?php echo get_template_directory_uri(); ?>https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" rel="stylesheet">
    <?php wp_head(); ?>   
  </head>
  <body <?php body_class(); ?>>
    <!-- Header -->
    <header class="shadow">
      <!-- Topbar -->
      <div class="py-3 text-sm bg-gray-700">
        <div class="container !mx-auto flex items-center justify-between">
          <!-- Secondary Links -->
          <ul class="secondary-menu">
            <li>
              <a href="#">Example Link</a>
            </li>
            <li>
              <a href="#">Example Link</a>
            </li>
            <li>
              <a href="#">Example Link</a>
            </li>
            <li>
              <a href="#">Example Link</a>
            </li>
          </ul>
          <!-- Regular Text -->
          <div class="text-gray-200 !mt-0">
            Some Regular Text
          </div>
        </div>
      </div>
      <!-- Header midsection -->
      <div class="container !mx-auto flex !flex-nowrap relative items-center py-5 justify-between w-full">
        <!-- Text Logo -->
        <?php $logoimg = get_header_image(); ?>
        <a href="<?php echo site_url();?>"><img src="<?php echo $logoimg?>" class="logo"
        alt="logo"
        ></a>

        <form class="header-search-form">
          <input type="text" placeholder="Search">
        </form>
        <!-- Header Tools -->
        <div class="wp-block-udemy-plus-header-tools">
          <!-- Signin Modal Link -->
          <a class="signin-link open-modal" href="#signin-modal">
            <div class="signin-icon">
              <i class="bi bi-person-circle"></i>
            </div>
            <div class="signin-text">
              <small>Hello</small>
              My Account
            </div>
          </a>
        </div>
      </div>
      <!-- Primary Menu -->
      <div class="container !mx-auto flex-nowrap justify-start pb-4 primary-menu">
        <?php wp_nav_menu(array('
        theme_location'=>'primary-menu',
        'menu_class'=>'nav')) ?>
      </div>
    </header>