<?php
get_header();
add_theme_support( "title-tag" );
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <link rel="icon" type="image/svg+xml" href="<?php echo get_template_directory_uri(); ?>/public/favicon.svg" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title><?php wp_title(); ?></title>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="<?php echo get_template_directory_uri(); ?>https://fonts.googleapis.com/css2?family=Pacifico&family=Rubik:wght@300;400;500;700&display=swap" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/public/index.css">
</head>

<body>
  <!-- Main Content -->
  <main class="container !mx-auto my-16 grid grid-cols-2">
    <div>
      <!-- Entry-->
      <article class="flex border-b border-b-gray-200 pb-10 mb-10">
        <div class="grid grid-rows-2 gap-1">
          <p>post - blogs</p>
          <?php
          while (have_posts()) {
            the_post();
            $imagepath = wp_get_attachment_image_src(
              get_post_thumbnail_id(),
              'large'
            );
          ?>
            <div class="container-1">
              <div class="card-1">
                <div class="img-container-1">
                  <img src="<?php echo @$imagepath[0] ?>" />
                </div>
                <div class="card-content-1">
                  <h1 class="font-bold"><?php the_title(); ?> </h1>
                  <!-- <h1>From the Other Side of the World</h1> -->
                  <p class="excerpt">
                    <?php the_excerpt(); ?> </p>
                  <?php echo get_the_date(); ?>
                  <p class="author-1 font-bold"> <?php the_author(); ?></p>
                  <a href="<?php the_permalink(); ?>">
                    <input class="cursor-pointer button" type="button" value="read more"></a>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
        <div class="clear"></div>
        <div class="pagination flex justify-between">
          <?php wp_pagenavi(); ?>
        </div>
      </article>
    </div>
    <div>
      <p>custom post - news</p>
      <div>
        <?php
        $wpnew = array(
          'post_type' => 'news',
          'post_status' => 'publish'
        );
        $newsquery = new WP_Query($wpnew);
        while ($newsquery->have_posts()) {
          $newsquery->the_post();
          $imagepath1 = wp_get_attachment_image_src(
            get_post_thumbnail_id(),
            'large'
          );
        ?>
          <div class="container-1">
            <div class="card-1">
              <div class="img-container-1">
                <img src="<?php echo @$imagepath1[0] ?>" />
              </div>
              <div class="card-content-1">
                <h1 class="font-bold"><?php the_title(); ?> </h1>
                <!-- <h1>From the Other Side of the World</h1> -->
                <p class="excerpt">
                  <?php the_excerpt(); ?> </p>
                <?php echo get_the_date(); ?>
                <p class="author-1 font-bold"> <?php the_author(); ?></p>
                <a href="<?php the_permalink(); ?>">
                  <input class="cursor-pointer button" type="button" value="read more"></a>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <div class="clear"></div>
        <div class="pagination flex justify-between">
          <?php wp_pagenavi(); ?>
        </div>
    </div>
  </main>
  <?php
  get_footer();
  ?>
</body>