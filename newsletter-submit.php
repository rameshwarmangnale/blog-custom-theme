
<?php
if(isset($_POST['email'])) {
  $email = $_POST['email'];
  
  // Replace with your MailChimp API key and list ID
  $apiKey = 'a0d05a3d4c0083ae396336be7c60cac8-us9';
  $listId = '8775507f6e';

  // Create a new instance of the MailChimp client
  $client = new /DrewM/MailChimp/MailChimp($apiKey);

  // Add the email address to the mailing list
  $result = $client->post("lists/$listId/members", [
    'email_address' => $email,
    'status'        => 'subscribed'
  ]);
  
  // Check if the email was added successfully
  if($client->success()) {
    // Once the email address is added to the mailing list, you can redirect the user to a thank you page
    header('Location: /thank-you');
    exit();
  } else {
    // Handle errors
    echo $client->getLastError();
  }
}
?>
<script>
    document.getElementById("newsletter-form").addEventListener("submit", function(event){
        event.preventDefault();
        var email = document.getElementById("email").value;
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "newsletter-submit.php", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200) {
                window.location = '/thank-you';
            }
        };
        xhr.send("email=" + email);
    });
</script>
