<?php

if ( is_active_sidebar( 'custom-widget-area' ) ) : ?>
    <div id="custom-widget-area" class="widget-area" role="complementary">
        <?php dynamic_sidebar( 'custom-widget-area' ); ?>
    </div>
<?php endif;

