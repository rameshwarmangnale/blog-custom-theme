<?php
get_header();

/* Start the Loop */
while (have_posts()) : the_post();
?>
   <div class="grid grid-cols-2 gap-4">
      <div class="col-span-2">
         <h1 class="font-bold text-2xl"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h1>
         <?php $imagepath = wp_get_attachment_image_src(
            get_post_thumbnail_id(),
            'large'
         ); ?>
         <br>
         <br>
         <img class="single-image" src="<?php echo $imagepath[0] ?>" width="600" height="400" />
         <div class="single-page">
            <p><?php echo get_the_date(); ?></p>
            <p> <?php the_author(); ?> </p>
         </div>
         <?php the_content(); ?>
         <p class="font-bold pt-5 pb-5"> comments below
         </p>
         <?php comments_template(); ?>
      </div>
   </div>
   <?php wp_reset_postdata(); ?>

<?php endwhile; ?>

<?php
get_footer();
?>