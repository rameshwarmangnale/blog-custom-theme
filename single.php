<?php
get_header();
while (have_posts()) : the_post();
?>

<main class="container !mx-auto my-16 grid grid-cols-3 gap-16">
    <div class="col-span-2">
        <!-- Entry-->
        <article class="flex border-b border-b-gray-200 pb-10 mb-10">
            <div class="grid grid-cols-2 gap-4">
                <div class="col-span-2">
                  <h1 class="font-bold text-2xl"> <?php the_title(); ?></h1>
                 <?php  $imagepath = wp_get_attachment_image_src(
                get_post_thumbnail_id(),'large'); ?>
                <br>
                <br>
                <?php if($imagepath!=""){ ?>
                <img class="single-image" src="<?php echo $imagepath[0] ?>"
                width="600" height="400"/>
                <?php } ?> 
                 <div class="single-page">
                 <p><?php echo get_the_date(); ?></p>
                <p> <?php the_author(); ?> </p>
                 </div>
                 <?php the_content(); ?>
                 <p class="font-bold pt-5 pb-5"> comments below
                 </p>
                <?php comments_template(); ?>  
                </div>
            </div>
        </article>
    </div>
    <?php endwhile; ?>
    <div>
        <!-- Popular tags-->
        <div class="sidebar-tags">
            <h6 class="text-xl font-medium mb-5">Blog Tags</h6>
            <a href="#">fashion</a>
            <a href="#">gadgets</a>
            <a href="#">online shopping</a>
            <a href="#">top brands</a>
            <a href="#">travel</a>
            <a href="#">cartzilla news</a>
            <a href="#">personal finance</a>
            <a href="#">tips &amp; tricks</a>
        </div>
        <!-- Trending posts-->
        <div class="mb-8">
            <h6 class="text-xl font-medium mb-5">Trending Posts</h6>
            <div class="flex mb-4 items-center">
                <!-- Post Image -->
                <a class="shrink-0" href="#">
                    <img class="rounded-lg w-16" src="<?php echo get_template_directory_uri(); ?>/public/trending-01.jpg">
                </a>
                <div class="text-sm ml-4">
                    <!-- Post Title -->
                    <a class="block font-medium" href="blog-single.html">
                        Retro Cameras are Trending. Why so Popular?
                    </a>
                    <!-- Post Author -->
                    <span class="text-gray-500">
                        by <a href="#">Andy Williams</a>
                    </span>
                </div>
            </div>
            <div class="flex mb-4 items-center">
                <a class="shrink-0" href="#">
                    <img class="rounded-lg w-16" src="<?php echo get_template_directory_uri(); ?>/public/trending-02.jpg">
                </a>
                <div class="text-sm ml-4">
                    <a class="block font-medium" href="blog-single.html">
                        Retro Cameras are Trending. Why so Popular?
                    </a>
                    <span class="text-gray-500">
                        by <a href="#">Andy Williams</a>
                    </span>
                </div>
            </div>
            <div class="flex mb-4 items-center">
                <a class="shrink-0" href="#">
                    <img class="rounded-lg w-16" src="<?php echo get_template_directory_uri(); ?>/public/trending-03.jpg">
                </a>
                <div class="text-sm ml-4">
                    <a class="block font-medium" href="blog-single.html">
                        Retro Cameras are Trending. Why so Popular?
                    </a>
                    <span class="text-gray-500">
                        by <a href="#">Andy Williams</a>
                    </span>
                </div>
            </div>
        </div>
        <!-- Categories-->
        <div class="sidebar-categories">
            <h6 class="text-xl font-medium mb-5">Blog Categories</h6>
            <ul>
                <li><a href="#">Example Category</a></li>
                <li><a href="#">Example Category</a></li>
                <li><a href="#">Example Category</a></li>
                <li><a href="#">Example Category</a></li>
                <li><a href="#">Example Category</a></li>
            </ul>
        </div>
    </div>
</main>

<?php
get_footer();
?>