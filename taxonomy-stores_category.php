<?php
get_header();

$catData = get_queried_object();

?>

<body>
    <!-- Main Content -->
    <main class="container !mx-auto my-16 grid grid-cols-3 gap-16">
        <div class="col-span-2">
            <!-- Entry-->
            <article class="flex border-b border-b-gray-200 pb-10 mb-10">
                <div class="grid grid-cols-3 gap-4">
                    <div class="col-span-2">
                        <h1 class="font-bold text-3xl pl-10"><?php echo $catData->name; ?></h1>

                        <!-- <img src="<?php echo $meta_image; ?>" class="w-28 h-28"> -->

                        <div class="col-span-2">
                            <?php
                            $wpnew = array(
                                'post_type' => 'stores',
                                'post_status' => 'publish',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'stores_category',
                                        'field' => 'term_id',
                                        'terms' => $catData->term_id
                                    )
                                ),
                            );
                            $storesquery = new WP_Query($wpnew);
                            while ($storesquery->have_posts()) {
                                $storesquery->the_post();
                                $imagepath1 = wp_get_attachment_image_src(
                                    get_post_thumbnail_id(),
                                    'large'
                                );
                            ?>
                                <div class="container-1">
                                    <div class="card-1">
                                        <div class="img-container-1">
                                            <img src="<?php echo @$imagepath1[0] ?>" />
                                        </div>
                                        <div class="card-content-1">
                                            <h1 class="font-bold"><?php the_title(); ?> </h1>
                                            <!-- <h1>From the Other Side of the World</h1> -->
                                            <p class="excerpt">
                                                <?php the_excerpt(); ?> </p>
                                            <?php echo get_the_date(); ?>
                                            <p class="author-1 font-bold"> <?php the_author(); ?></p>
                                            <a href="<?php the_permalink(); ?>">
                                                <input class="cursor-pointer button" type="button" value="read more"></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </article>
        </div>
        </div>
    </main>
    <?php
    get_footer();
    ?>
</body>