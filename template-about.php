<?php
//Template Name:about us
get_header();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/svg+xml" href="<?php echo get_template_directory_uri(); ?>/public/favicon.svg" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Udemy Static Template</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="<?php echo get_template_directory_uri(); ?>https://fonts.googleapis.com/css2?family=Pacifico&family=Rubik:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/public/index.css">
</head>

<body>
    <div class="container !mx-auto my-16 grid grid-row-2 gap-16 justify-center">
        <div class="grid grid-row-2 justify-center !mx-auto">
            <h1 class="row justify-content-center font-bold"> About WisdmLabs </h1>
            <p>We’re a team of engineers, client success managers, product creators and visionaries who’ve architected some awesome solutions on open source!</p>
            <h1>The visionaries at the helm</h1>
        </div>
        <article class="flex border-b border-b-gray-200 pb-10 mb-10 justify-center ">
            <div class="grid grid-cols-3 gap-4 row justify-content-center !mx-auto">
                <div>
                    <!-- Author -->
                    <a class="post-author" href="#">
                        <img src="<?php echo get_template_directory_uri(); ?>/public/rohan-thakre.jpg" alt="Emma Gallaher">
                        <span>Rohan Thakare</span>
                    </a>
                    <!-- Title -->
                    <h2 class="text-xl mb-4 font-medium text-gray-700">
                        <a href="#">
                            – Rohan Thakare, Founder
                            ex-IBM, ex-Convonix, avid trekker and foodie at heart. Reigning debate champion at Wisdm
                        </a>
                    </h2>
                </div>
                <div class="col-span-2">

                    <span class=" rounded-md">
                        <?php the_post_thumbnail('large'); ?>
                    </span>
                    <p class="post-excerpt text-sm pt-5">
                    <p>
                        Back in 2012, I started the company with two employees in a make-shift office. I had a clear mission — to create value for all our stake-holders — employees and clients alike.

                        And as a team, we’ve worked passionately to deliver on this, by creating quality applications for our clients and building sustainable careers for our colleagues.

                        Today, I’m proud to say that we’ve garnered industry recognition and have grown to a 100+ member team spread across India (since we work remotely, of course!)
                    </p>
                    <a href="#">
                        [Read more]
                    </a>
                    </p>
                </div>
            </div>
        </article>
    </div>
</body>

</html>
<?php
get_footer();
?>