<?php
//Template Name:Home
get_header();
?>
<!-- <?php $args = array(
     'public'   => true,
     '_builtin' => false
);?>
<?php $show_post_types = get_post_types( $args ); ?> 
<pre>
  <?php print_r($show_post_types); ?> 
</pre>  -->
<main class="container !mx-auto my-16 grid grid-cols-2 gap-16">
  <div>
    News Category
    <?php $newsCat = get_terms([
      'taxonomy' => 'news_category', 'hide_empty' => false,
      'orderby' => 'name', 'order' => 'DESC', 'parent' => 0
    ]);
    foreach ($newsCat as $newsCatData) {
      $meta_image = get_wp_term_image(
        $newsCatData->term_id
      );
    ?>
      <!-- Entry-->
      <article class="flex border-b justify-center border-b-gray-200 pb-10 mb-10">
        <div class="col-span-2">
          <!-- Post Image -->
          <a class="rounded-lg mb-4" href="<?php echo get_category_link($newsCatData->term_id) ?>"> 
         <?php if($meta_image!=""){ ?>
          <img src="<?php print_r($meta_image); ?>" class="w-28 h-28">
        <?php } ?> 
        </a>

          <!-- Post Excerpt -->
          <p class="post-excerpt text-sm">
            <a class=" " href="<?php echo get_category_link($newsCatData->term_id) ?>">
              <h1 class="font-bold text-3xl">News-Category - <?php echo $newsCatData->name; ?> </h1>
            </a>
          </p>
        </div>
      </article>
    <?php  } ?>
  </div>
  <div>
    <!-- Popular tags-->
    Store Category
    <?php $storesCat = get_terms([
      'taxonomy' => 'stores_category', 'hide_empty' => false,
      'orderby' => 'name', 'order' => 'DESC', 'parent' => 0
    ]);
    foreach ($storesCat as $storesCatData) {
      $meta_image = get_wp_term_image(
        $storesCatData->term_id
      );
    ?>
      <!-- Entry-->
      <article class="flex border-b justify-center border-b-gray-200 pb-10 mb-10">
        <div class="col-span-2">
          <!-- Post Image -->
          <a class="rounded-lg mb-4" href="<?php echo get_category_link($storesCatData->term_id) ?>">
            <img src="<?php print_r($meta_image); ?>" class="w-36 h-36">
          </a>
          <!-- Post Excerpt -->
          <p class="post-excerpt text-sm">
            <a class=" " href="<?php echo get_category_link($storesCatData->term_id) ?>">
              <h1 class="font-bold text-3xl">Stores-Category - <?php echo $storesCatData->name; ?> </h1>
            </a>
          </p>
        </div>
      </article>
    <?php  } ?>
  </div>
</main>
<?php

get_footer();
?>