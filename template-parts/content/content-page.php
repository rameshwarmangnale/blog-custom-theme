<?php
get_header();
the_post();
?>
<div class="container !mx-auto grid grid-cols-2 ">
<h1><a  href="<?php echo site_url(); ?> "> <i>Home </i> </a></h1>
<h1><?php the_title(); ?> </h1>
</div>
<main class="container !mx-auto my-16 grid grid-cols-3 gap-16">
      <div class="col-span-2">
        <!-- Entry-->
        <article class="flex border-b border-b-gray-200 pb-10 mb-10">
          <div class="grid grid-cols-3 gap-4">
            <div>
              <!-- Author -->
              <a class="post-author" href="#">
                <img src="<?php echo get_template_directory_uri(); ?>/public/author.jpg" alt="Emma Gallaher">
                <span>Rameshwar Mangnale</span>
              </a>
              <!-- Title -->
              <h2 class="text-xl mb-4 font-medium text-gray-700">
                <a href="#">
                  We Launched Regular Drone Delivery in California. 
                  Watch Demo Video
                </a>
              </h2>
              <!-- Date -->
              <span class="text-gray-500">Dec 30</span>
            </div>
            <div class="col-span-2">
              <!-- Post Image -->
              <!-- <a class="block rounded-lg overflow-hidden mb-4" href="#">
                <img src="/public/blog-post-01.jpg" class="w-full">
              </a>
              <div class="flex justify-between pb-4 text-gray-500 text-sm"> -->
                <!-- Post Tags -->
                <!-- <div>
                  <a href="#" class="blog-entry-meta-link">Travel</a>, 
                  <a href="#" class="blog-entry-meta-link">Personal finance</a>
                </div> -->
                <!-- Post Comment Count -->
                <!-- <div>
                  <a class="bi post-comment-count !mt-0" href="#comments">
                    8
                  </a>
                </div> -->
              <!-- </div> -->
              <!-- Post Excerpt -->
              <span class=" rounded-md">
              <?php the_post_thumbnail('large'); ?>
              </span>
              <p class="post-excerpt text-sm pt-5">
                
                <?php the_content(); ?>
                
                <a href="#">
                  [Read more]
                </a>
              </p>
            </div>
          </div>
          </article>
        <!-- Pagination-->
        <nav class="flex justify-between">
          <a class="rounded-md py-2 px-4 block transition-all hover:bg-gray-100" href="#">
            Prev Page
          </a>
          <a class="rounded-md py-2 px-4 block transition-all hover:bg-gray-100" href="#">
            Next Page
          </a>
        </nav>
      </div>
      <div>
        <!-- Popular tags-->
        <div class="sidebar-tags">
          <h6 class="text-xl font-medium mb-5">Blog Tags</h6>
          <a href="#">fashion</a>
          <a href="#">gadgets</a>
          <a href="#">online shopping</a>
          <a href="#">top brands</a>
          <a href="#">travel</a>
          <a href="#">cartzilla news</a>
          <a href="#">personal finance</a>
          <a href="#">tips &amp; tricks</a>
        </div>
        <!-- Trending posts-->
        <div class="mb-8">
          <h6 class="text-xl font-medium mb-5">Trending Posts</h6>
          <div class="flex mb-4 items-center">
            <!-- Post Image -->
            <a class="shrink-0" href="#">
              <img class="rounded-lg w-16" src="/public/trending-01.jpg">
            </a>
            <div class="text-sm ml-4">
              <!-- Post Title -->
              <a class="block font-medium" href="blog-single.html">
                Retro Cameras are Trending. Why so Popular?
              </a>
              <!-- Post Author -->
              <span class="text-gray-500">
                by <a href="#">Andy Williams</a>
              </span>
            </div>
          </div>
          <div class="flex mb-4 items-center">
            <a class="shrink-0" href="#">
              <img class="rounded-lg w-16" src="/public/trending-02.jpg">
            </a>
            <div class="text-sm ml-4">
              <a class="block font-medium" href="blog-single.html">
                Retro Cameras are Trending. Why so Popular?
              </a>
              <span class="text-gray-500">
                by <a href="#">Andy Williams</a>
              </span>
            </div>
          </div>
          <div class="flex mb-4 items-center">
            <a class="shrink-0" href="#">
              <img class="rounded-lg w-16" src="/public/trending-03.jpg">
            </a>
            <div class="text-sm ml-4">
              <a class="block font-medium" href="blog-single.html">
                Retro Cameras are Trending. Why so Popular?
              </a>
              <span class="text-gray-500">
                by <a href="#">Andy Williams</a>
              </span>
            </div>
          </div>
        </div>
        <!-- Categories-->
        <div class="sidebar-categories">
          <h6 class="text-xl font-medium mb-5">Blog Categories</h6>
          <ul>
            <li><a href="#">Example Category</a></li>
            <li><a href="#">Example Category</a></li>
            <li><a href="#">Example Category</a></li>
            <li><a href="#">Example Category</a></li>
            <li><a href="#">Example Category</a></li>
          </ul>
        </div>
      </div>
    </main>
<?php
get_footer();
?>
